export class AcademicTeacher {
  id?: number;
  degree: AcademicDegree;
  name: string;
  surname: string;

  constructor(id: number, degree: AcademicDegree, name: string, surname: string) {
    this.id = id;
    this.degree = degree;
    this.name = name;
    this.surname = surname;
  }

  toString(): string {
    switch (this.degree) {
      case 'ENGINEER':
        return 'inż.' + ' ' + this.name + ' ' + this.surname;
      case 'MASTER':
        return 'mgr' + ' ' + this.name + ' ' + this.surname;
      case 'DOCTOR':
        return 'dr' + ' ' + this.name + ' ' + this.surname;
      case 'DOCTOR_ENGINEER':
        return 'dr inż.' + ' ' + this.name + ' ' + this.surname;
      case 'HABILITATION':
        return 'dr hab.' + ' ' + this.name + ' ' + this.surname;
      case 'ASSOCIATE_PROFESSOR':
        return 'prof. nadzw.' + ' ' + this.name + ' ' + this.surname;
      case 'PROFESSOR':
        return 'prof.' + ' ' + this.name + ' ' + this.surname;
    }
  }

  canBeChairman(): boolean {
    return this.degree === 'PROFESSOR' || this.degree === 'ASSOCIATE_PROFESSOR' || this.degree === 'HABILITATION';
  }
}

export interface Committee {
  id?: number,
  chairman?: AcademicTeacher,
  members: AcademicTeacher[]
}

export class Auditing {
  id?: number;
  dateOfCompletion: Date;
  status?: AuditingStatus = 'COMPLEX';
  committee: Committee;
  audited: AcademicTeacher;
  plan?: Plan;
  group: Group;
}

export interface Group {
  id?: number,
  name: string,
  code: string,
  places: number,
  totalPlaces: number,
  room: string,
  building: string,
  dates: Date[],
  academicTeacher: AcademicTeacher
}

export interface Document {
  id?: number,
  documentNumber: string
}

export class Semester {
  id?: number;
  type: SemesterType;
  beginYear: number;

  constructor(id: number, type: SemesterType, beginYear: number) {
    this.id = id;
    this.type = type;
    this.beginYear = beginYear;
  }

  get endYear(): number {
    return this.beginYear + 1;
  }

  toString(): string {
    switch (this.type) {
      case 'SUMMER':
        return 'Letni ' + (this.beginYear % 100) + '/' + (this.endYear % 100);
      case 'WINTER':
        return 'Zimowy ' + (this.beginYear % 100) + '/' + (this.endYear % 100);
    }
  }
}

export interface Plan extends Document {
  accepted: boolean,
  auditing: Auditing[],
  semester: Semester
}


export type AuditingStatus =
  'COMPLEX'
  | 'EVALUATED'
  | 'ACCEPTED'
  | 'REJECTED'
  | 'FINISHED';

export type Grade =
  'DISTINCTIVELY'
  | 'WELL'
  | 'ENOUGH'
  | 'INSUFFICIENTLY'
  | 'NOT_APPLICABLE';

export type AcademicDegree =
  'ENGINEER'
  | 'MASTER'
  | 'DOCTOR'
  | 'DOCTOR_ENGINEER'
  | 'HABILITATION'
  | 'ASSOCIATE_PROFESSOR'
  | 'PROFESSOR';

export type SemesterType = 'WINTER' | 'SUMMER';
