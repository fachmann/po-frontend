import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PlanBrowseComponent} from './plan-browse.component';

describe('PlanBrowseComponent', () => {
  let component: PlanBrowseComponent;
  let fixture: ComponentFixture<PlanBrowseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanBrowseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanBrowseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
