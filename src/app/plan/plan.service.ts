import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import {HttpClient} from '@angular/common/http';
import {AcademicTeacher, Auditing, Committee, Group, Plan, Semester} from '../domain-model/model';
import 'rxjs/add/operator/first';

@Injectable()
export class PlanService {

  semesters: Semester[] = [
    new Semester(0, 'WINTER', 2016),
    new Semester(1, 'SUMMER', 2016),
    new Semester(2, 'WINTER', 2017),
    new Semester(3, 'SUMMER', 2017)
  ];

  teachers: AcademicTeacher[] = [
    new AcademicTeacher(0, 'ASSOCIATE_PROFESSOR', 'Jerzy', 'Zdrowaś'),
    new AcademicTeacher(1, 'HABILITATION', 'Małgorzata', 'Wróbel'),
    new AcademicTeacher(2, 'PROFESSOR', 'Adam', 'Małysz'),
    new AcademicTeacher(3, 'DOCTOR', 'Kamil', 'Stoch'),
    new AcademicTeacher(4, 'ENGINEER', 'Maciej', 'Kot'),
    new AcademicTeacher(5, 'MASTER', 'Stefan', 'Hula'),
    new AcademicTeacher(6, 'MASTER', 'Piotr', 'Żyła'),
    new AcademicTeacher(7, 'DOCTOR', 'Stefan', 'Horngacher'),
    new AcademicTeacher(8, 'DOCTOR', 'Dawid', 'Kubacki'),
    new AcademicTeacher(9, 'HABILITATION', 'Andrzej', 'Duda'),
    new AcademicTeacher(10, 'ASSOCIATE_PROFESSOR', 'Ewa', 'Kopacz')
  ];
  committees: Committee[] = [
    {
      id: 0,
      members: [
        this.teachers[1],
        this.teachers[0],
        this.teachers[2]
      ],
      chairman: this.teachers[0]
    },
    {
      id: 1,
      members: [
        this.teachers[1],
        this.teachers[4],
        this.teachers[0]
      ],
      chairman: this.teachers[1]
    },
    {
      id: 2,
      members: [
        this.teachers[7],
        this.teachers[1],
        this.teachers[2]
      ],
      chairman: this.teachers[2]
    },
    {
      id: 3,
      members: [
        this.teachers[3],
        this.teachers[4],
        this.teachers[1]
      ],
      chairman: this.teachers[1]
    }
  ];
  groups: Group[] = [
    {
      id: 0,
      name: 'Projektowanie oprogramowania',
      code: 'Z00-30a',
      places: 12,
      totalPlaces: 16,
      building: 'D-1',
      room: '29',
      academicTeacher: this.teachers[0],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 1,
      name: 'Podstawy Inżynierii Oprogramowania',
      code: 'Z00-45u',
      places: 12,
      totalPlaces: 16,
      building: 'D-2',
      room: '209',
      academicTeacher: this.teachers[1],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 2,
      name: 'Bazy danych',
      code: 'Z00-11a',
      places: 12,
      totalPlaces: 16,
      building: 'D-1',
      room: '29',
      academicTeacher: this.teachers[2],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 3,
      name: 'Podstawy Inżynierii Oprogramowania',
      code: 'Z00-43g',
      places: 12,
      totalPlaces: 16,
      building: 'D-2',
      room: '209',
      academicTeacher: this.teachers[3],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 4,
      name: 'Projektowanie oprogramowania',
      code: 'Z00-23q',
      places: 12,
      totalPlaces: 16,
      building: 'D-1',
      room: '29',
      academicTeacher: this.teachers[4],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 5,
      name: 'Podstawy Inżynierii Oprogramowania',
      code: 'Z00-45g',
      places: 12,
      totalPlaces: 16,
      building: 'D-2',
      room: '209',
      academicTeacher: this.teachers[5],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 6,
      name: 'Projektowanie oprogramowania',
      code: 'Z00-23e',
      places: 12,
      totalPlaces: 16,
      building: 'D-1',
      room: '29',
      academicTeacher: this.teachers[6],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 7,
      name: 'Podstawy Inżynierii Oprogramowania',
      code: 'Z00-45c',
      places: 12,
      totalPlaces: 16,
      building: 'D-2',
      room: '209',
      academicTeacher: this.teachers[7],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 8,
      name: 'Projektowanie oprogramowania',
      code: 'Z00-23d',
      places: 12,
      totalPlaces: 16,
      building: 'C-6',
      room: '29',
      academicTeacher: this.teachers[8],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    },
    {
      id: 9,
      name: 'Podstawy Inżynierii Oprogramowania',
      code: 'Z00-45a',
      places: 12,
      totalPlaces: 16,
      building: 'C-13',
      room: '209',
      academicTeacher: this.teachers[9],
      dates: [this.randomDate(), this.randomDate(), this.randomDate(), this.randomDate()]
    }
  ];
  auditing: Auditing[] = [
    {
      id: 0,
      dateOfCompletion: this.randomDate(),
      status: 'COMPLEX',
      group: this.groups[0],
      committee: this.committees[0],
      audited: this.teachers[5]
    },
    {
      id: 1,
      dateOfCompletion: this.randomDate(),
      status: 'COMPLEX',
      group: this.groups[1],
      committee: this.committees[1],
      audited: this.teachers[6]
    },
    {
      id: 2,
      dateOfCompletion: this.randomDate(),
      status: 'COMPLEX',
      group: this.groups[2],
      committee: this.committees[2],
      audited: this.teachers[7]
    },
    {
      id: 3,
      dateOfCompletion: this.randomDate(),
      status: 'COMPLEX',
      group: this.groups[3],
      committee: this.committees[3],
      audited: this.teachers[8]
    }
  ];

  plans: Plan[] = [
    {
      id: 0,
      accepted: false,
      documentNumber: 'ABC/001',
      semester: this.semesters[0],
      auditing: [
        this.auditing[0],
        this.auditing[1],
        this.auditing[2],
        this.auditing[3]
      ]
    },
    {
      id: 1,
      accepted: true,
      documentNumber: 'ABD/002',
      semester: this.semesters[1],
      auditing: [
        this.auditing[0],
        this.auditing[1],
        this.auditing[2],
        this.auditing[3]
      ]
    }
  ];

  constructor(private http: HttpClient) {
  }

  getSemesters(): Observable<Semester[]> {
    return Observable.of(this.semesters);
  }

  getTeachers(): Observable<AcademicTeacher[]> {
    return Observable.of(this.teachers);
  }

  getGroups(): Observable<Group[]> {
    return Observable.of(this.groups);
  }

  getPlan(id: number): Observable<Plan> {
    const filteredPlans = this.plans.filter(value => value.id === id);
    return Observable.of(filteredPlans.length > 0 ? filteredPlans[0] : null);
  }

  getPlans(): Observable<Plan[]> {
    return Observable.of(this.plans);
  }

  private randomDate(): Date {
    return new Date(2017,
      Math.floor(Math.random() * 4 + 8),
      Math.floor(Math.random() * 29 + 1),
      Math.floor(Math.random() * 4 + 4) * 2 + 1, 15, 0, 0);
  }
}
