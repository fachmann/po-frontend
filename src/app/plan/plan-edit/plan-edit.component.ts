import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AcademicTeacher, Auditing, Committee, Plan} from '../../domain-model/model';
import {PlanService} from '../plan.service';
import {SelectItem} from 'primeng/primeng';

@Component({
  selector: 'app-model-search',
  templateUrl: './plan-edit.component.html',
  styleUrls: ['./plan-edit.component.scss']
})
export class PlanEditComponent implements OnInit {

  plan: Plan = null;

  teachers: SelectItem[] = null;
  filteredTeachers: SelectItem[] = null;
  newTeacher: any = null;

  groups: SelectItem[] = null;
  filteredGroups: SelectItem[] = null;
  newGroup: any = null;

  dates: SelectItem[] = null;
  dateOfCompletion: Date = null;

  newCommittee: Committee = {members: []};


  constructor(private route: ActivatedRoute, private planService: PlanService) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      const id = Number(params.id);
      if (!isNaN(id)) {
        this.getPlan(id);
      }
    });
  }

  searchTeacher(event: any, actualMembers: AcademicTeacher[] = []): void {
    this.filteredTeachers = [...this.teachers.filter(
      teacher => teacher.label.toLowerCase().includes(event.query.toLowerCase()) &&
        !actualMembers.map(member => member.id).includes(teacher.value.id)
    )];
  }

  searchGroup(event: any): void {
    this.filteredGroups = [...this.groups.filter(
      group => group.label.toLowerCase().includes(event.query.toLowerCase()) &&
        !this.plan.auditing.map(auditing => auditing.group.code).includes(group.value.code)
    )];
  }

  deleteAuditing(i): void {
    this.plan.auditing = this.plan.auditing.filter((value, index) => i !== index);
  }

  setChairman(committee: Committee, member: AcademicTeacher) {
    committee.chairman = new AcademicTeacher(-1, null, null, null);
    setTimeout(() => {
      committee.chairman = member;
    }, 1);
  }

  addMember(committee: Committee, teacherItem: SelectItem, row: any = null) {
    if (teacherItem instanceof Object) {
      const teacher = teacherItem.value;
      if (committee.members.indexOf(teacher) === -1) {
        committee.members.push(teacher);
        if (row) {
          row.newTeacher = '';
        } else {
          this.newTeacher = null;
        }
      }
    }
  }

  deleteMember(committee: Committee, member: AcademicTeacher) {
    const index = committee.members.indexOf(member);
    committee.members.splice(index, 1);
    if (member === committee.chairman) {
      committee.chairman = null;
    }
  }

  addAuditing() {
    const newAuditing: Auditing = {
      status: 'ACCEPTED',
      audited: this.newGroup.value.academicTeacher,
      dateOfCompletion: this.dateOfCompletion,
      group: this.newGroup.value,
      committee: this.newCommittee
    };
    this.plan.auditing.push(newAuditing);
    this.plan.auditing = [...this.plan.auditing];
    this.newGroup = null;
    this.dateOfCompletion = null;
    this.newTeacher = null;
    this.newCommittee = {members: []};
  }

  refreshDates() {
    this.dates = this.getDates();
    this.dateOfCompletion = null;
  }

  getDates(): SelectItem[] {
    if (this.newGroup != null && this.newGroup.value != null && this.newGroup.value.dates != null) {
      return this.newGroup.value.dates.map((date: Date): SelectItem => (
        {
          label: this.formatDate(date),
          value: date
        }
      ));
    }
    return null;
  }

  formatDate(date: Date): string {
    if (date) {
      return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' - '
        + date.getHours() + ':' + date.getMinutes();
    }
    return '';
  }

  onResize(event): void {

  }

  private getPlan(id: number): void {
    this.planService.getTeachers().subscribe(teachers => {
      this.teachers = teachers.map(teacher => (
        {
          label: teacher.toString(),
          value: teacher
        }
      ));
      this.planService.getGroups().subscribe(groups => {
        this.groups = groups.map(group => (
          {
            label: group.code,
            value: group
          }
        ));
        this.planService.getPlan(id).subscribe(plan => this.plan = plan);
      });
    });
  }
}
