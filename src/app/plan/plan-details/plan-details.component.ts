import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Plan} from '../../domain-model/model';
import {PlanService} from '../plan.service';

@Component({
  selector: 'app-plan-details',
  templateUrl: './plan-details.component.html',
  styleUrls: ['./plan-details.component.scss']
})
export class PlanDetailsComponent implements OnInit {

  plan: Plan = null;

  constructor(private route: ActivatedRoute, private planService: PlanService) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      const id = Number(params.id);
      if (!isNaN(id)) {
        this.getPlan(id);
      }
    });
  }

  private getPlan(id: number): void {
    this.planService.getPlan(id).subscribe(plan => this.plan = plan);
  }
}
