import {Component, OnInit} from '@angular/core';

import {Plan, Semester} from '../../domain-model/model';
import {PlanService} from '../plan.service';
import {SelectItem} from 'primeng/primeng';

@Component({
  selector: 'app-plan-browse',
  templateUrl: './plan-browse.component.html',
  styleUrls: ['./plan-browse.component.scss']
})
export class PlanBrowseComponent implements OnInit {

  plans: Plan[];
  semesters: SelectItem[] = [{label: 'Dowolny', value: ANY_SEMESTER}];
  filteredPlans: Plan[];

  filterOptions: FilterOptions = new FilterOptions();

  constructor(private planService: PlanService) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData(): void {
    this.planService.getPlans().subscribe(plans => {
      this.plans = this.filteredPlans = plans;
    });
    this.planService.getSemesters().subscribe(semesters => {
      this.semesters = this.semesters.concat(semesters.map(item =>
        ({label: item.toString(), value: item} as SelectItem)
      ));
    });
  }

  filter(): void {
    this.filteredPlans = this.plans.filter(plan => {
      return (this.filterOptions.semester.id === -1 || plan.semester.id === this.filterOptions.semester.id) &&
        plan.documentNumber.toLowerCase().includes(this.filterOptions.documentNumber.toLowerCase());

    })
  }

  resetFilter(): void {
    this.filterOptions.reset();
    this.filter();
  }

  onResize(event): void {

  }
}


export const
  ANY_SEMESTER: Semester = new Semester(-1, 'WINTER', 0);

export class FilterOptions {
  documentNumber = '';
  semester: Semester;

  constructor() {
    this.reset();
  }

  reset() {
    this.documentNumber = '';
    this.semester = ANY_SEMESTER;
  }
}
